from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import (Layout, HTML)
from crispy_forms.bootstrap import (StrictButton, Field, Div)


class UploadMapForm(forms.Form):
    title = forms.CharField(max_length=100, label='Nome')
    file = forms.FileField(label='Arquivo')

    def __init__(self, *args, **kwargs):
        super(UploadMapForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)

        self.helper.form_class = 'horizontal_form'
        self.helper.form_method = 'post'
        self.helper.form_action = 'save_map'

        self.helper.layout = Layout(
            Field('title'),
            Field('file'),
            StrictButton('Enviar', css_class='btn btn-default', type='submit'),
        )


class SearchCostForm(forms.Form):
    map_name = forms.CharField(max_length=200, label='Mapa')
    origin = forms.CharField(max_length=200, label='Origem')
    destination = forms.CharField(max_length=200, label='Destino')
    vehicle_autonomy = forms.IntegerField(min_value=0, label='Autonomia')
    oil_price = forms.DecimalField(min_value=0, label='Combustivel')

    def __init__(self, *args, **kwargs):
        super(SearchCostForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)

        self.helper.form_class = 'horizontal_form'
        self.helper.form_method = 'post'
        self.helper.form_action = 'get_cost'

        self.helper.layout = Layout(
            Field('map_name'),
            Field('origin'),
            Field('destination'),
            Field('vehicle_autonomy'),
            Field('oil_price'),
            StrictButton('Enviar', css_class='btn btn-default', type='submit'),
        )