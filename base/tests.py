from django.test import TestCase


class UrlsTestCase(TestCase):
    def test_home(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_about(self):
        response = self.client.get('/about')
        self.assertEqual(response.status_code, 200)

    def test_contact(self):
        response = self.client.get('/contact')
        self.assertEqual(response.status_code, 200)

    def test_map_url(self):
        response = self.client.get('/mapa/')
        self.assertEqual(response.status_code, 200)

    def test_webservice_url(self):
        response = self.client.get('/ws/')
        self.assertEqual(response.status_code, 200)