"""
    References
    Graphs: http://networkx.github.io/documentation/latest/reference/introduction.html
            http://networkx.github.io/documentation/latest/reference/readwrite.html
"""

from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from forms import UploadMapForm, SearchCostForm
from django.conf import settings

import networkx as nx
import os


def home(request):
    map_form = UploadMapForm()
    search_cost_form = SearchCostForm()
    maps = get_maps()
    forms = {'map_form':map_form,
             'search_cost_form': search_cost_form,
             'maps':maps}
    return render(request, 'main.html', forms)


def about(request):
    return render(request, 'about.html', {})


def contact(request):
    return render(request, 'contact.html', {})


def get_cost(request):
    if request.method == 'POST':
        form = SearchCostForm(request.POST, request.FILES)
        if form.is_valid():
            data = form.cleaned_data
            path, cost = calculate_cost(**data)
            data = {'path': ' -> '.join(path),
                    'cost': cost}
            return render(request, 'show_cost.html',data)

    return HttpResponseRedirect('/')


def ws_cost(request):
    if request.method == 'POST':
            # TODO: validate JSON
            # TODO: if not valid respond according
            # TODO: if valid get data, calculate cost and respond with result
            return HttpResponse("json_com_resposta")
    return HttpResponse("json_com_resposta")


def calculate_cost(map_name, origin, destination, vehicle_autonomy, oil_price):
    graph_file_path = normalize_map_file_path(map_name)
    G = nx.read_graphml(graph_file_path)
    graph_path = nx.dijkstra_path(G,origin,destination)
    graph_path_length = nx.dijkstra_path_length(G,origin,destination)
    cost = float(float(graph_path_length) / float(vehicle_autonomy)) * float(oil_price)
    return graph_path, cost


def get_maps():
    maps_path = get_maps_path()
    map_list = os.listdir(maps_path)

    maps = []
    for map_ in map_list:
        name, extension = map_.split('.')
        if extension == 'graphml':
            maps.append(name)
    return maps

def save_map(request):
    if request.method == 'POST':
        form = UploadMapForm(request.POST, request.FILES)
        if form.is_valid():
            map_name = form.cleaned_data.get('title')
            map_file = request.FILES['file']
            handle_map_file_upload(map_name, map_file)
            return render(request, 'map_success.html',{})

    return HttpResponseRedirect('/')


def handle_map_file_upload(name, file_):
    # TODO: try to identify various types of separators
    sep = ','
    edges = []

    for chunk in file_.chunks():
        for node in str.split(chunk):
            edge = ['{0}'.format(edge) for edge in node.split(sep)]
            edge = tuple(edge[0:-1] + [int(edge[-1])])
            edges.append(edge)

    G = nx.DiGraph()
    G.add_weighted_edges_from(edges)

    graph_file_path = normalize_map_file_path(name)
    nx.write_graphml(G, graph_file_path)

def get_maps_path():
    MAPS_DIR = getattr(settings, "MAPS_DIR", None)
    if not MAPS_DIR:
        MAPS_DIR = os.path.join(settings.BASE_DIR,'static', 'maps')

    return MAPS_DIR

def normalize_map_file_path(map_name):

    graph_file_path = os.path.join(get_maps_path(), map_name + '.graphml')

    return graph_file_path