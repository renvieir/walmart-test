# Teste Walmart  #

Este repositório foi criado para o teste de seleção para vaga de desenvolvedor python.

## No que consiste o teste? ##

Criar um sistema de logística onde será calculado o custo de uma entrega.
Nesse sistema um usuário deve poder subir um mapa com informações sobre distância ente cidades, não ficou claro como o usuário vai subir esses dados, eu supus que ele vai subir um arquivo, visto que não seria eficiente subir uma entrada de cada vez.

Também é necessário que o usuário possa fazer consultas sobre o menor custo de entrega de um ponto A a outro ponto B, bem como a rota entre os pontos. 
Para fazer essas consultas o usuário entrará com os dados:

* mapa
* origem
* destino
* autonomia do veiculo (km/l)
* preco do combustivel

## Arquitetura Proposta ##

O sistema consistirá de um site com formulários de envio de mapa e de consulta de preços.

## Tecnologias e ferramentas utilizadas ##

O site foi feito utilizando o framework Django e o pacote django-crispy-forms e o framework Bootstrap.
Os dados foram modelados como grafos e para isso foi utilizado o pacote networkx, que constroi grafos e calcula o menor caminho utilizando o algoritmo de Djikstra.